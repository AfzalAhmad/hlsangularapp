import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User, Server, VideoArchiveQuery } from '../_models';
import { UserService, AlertService } from '../_services';
import { SelectItem } from 'primeng/api';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { EnvService } from '../_services/env.service'


interface Option1 {
    name: string;
    code: string;
}

declare function myMethod(): any;
declare function LoadPlaylistInPlayer(string): any;

@Component({ templateUrl: 'hls.component.html' })
export class HLSComponent implements OnInit {
    title = 'date-time-app';
    currentUser: User;   
    loadAPI: Promise<any>;   
    playlistUrl: string;
    noImage: boolean;
    noVideo:boolean;
    imagesrc: string = window.location.origin;
   
   
    constructor(       
        private route: ActivatedRoute,
        private env: EnvService,
        private alertService: AlertService) {
        this.currentUser = JSON.parse(localStorage.getItem('mpServerUser'));
        this.loadAPI = new Promise((resolve) => {
            
            resolve(true);
        });

        this.playlistUrl = "http://localhost:8080/1/mp_2020127_161639.m3u8";
        this.imagesrc = this.imagesrc + "/assets/images/servernovideo.jpeg";
        console.log(this.imagesrc);
       
        if(this.route.snapshot.queryParamMap.keys[0] == undefined)
        {
            this.noImage = true;
            this.noVideo = false;
        }
        else
        {
            this.noImage = false;
            this.noVideo = true;
        }
        
       
    }

    ngOnInit() {
        console.log(this.route.snapshot.queryParamMap.keys[0]);
        if(this.route.snapshot.queryParamMap.keys[0] == undefined)
        {
            //this.noImage = true;
            //this.noVideo = false;
            console.log(this.noImage);
        }
        else
        {
            //this.noImage = false;
            //this.noVideo = true;
            this.playlistUrl = this.env.nginxUrl+this.route.snapshot.queryParamMap.keys[0];
            LoadPlaylistInPlayer(this.playlistUrl);
        }
    }

   

   
   

   
}
