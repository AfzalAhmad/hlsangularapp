import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { UserService } from '../_services';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private userService: UserService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {
                // auto logout if 401 response returned from api
                this.userService.logout();
                location.reload(true);
            }
            var errortext =  JSON.stringify(err.error.ModelState);
            for (var key in err.error.ModelState) {
                for (var i = 0; i < err.error.ModelState[key].length; i++) {
                    errortext = err.error.ModelState[key][i];
                }
            }
            const error = errortext || err.error.message || err.statusText;
            
            return throwError(error);
        }))
    }
}