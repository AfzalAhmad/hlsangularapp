﻿export class User {
    UserName: string;
    Email: string;
    Password: string;
    ConfirmPassword: string;
    access_token: string;
}