﻿export class Server {
    // ServerID: string;
    // ServerName: string;
    // ServerPort: number;
    // ServerLocation: string;
    // ComputerName: string;
    // RegisterationTime: string;
    // Active: boolean;

    constructor(
        public ServerID: string,
        public ServerName: string,
        public ServerPort: number,
        public ServerLocation: string,
        public ComputerName: string,
        public RegisterationTime: string,
        public Active: boolean
    ) {}
}