export class Software {
    constructor(
    public Id: string,
    public FileUniqueName: string,
    public FileActualName: string,
    public ContentType: string,
    public FileExtension: string,
    public FileSize: number,
    public UploaddateTime: string,
    public Version: string,
    public Details: string
) {}
}