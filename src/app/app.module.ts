import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import { FileUploadModule } from 'primeng/fileupload';

import { AlertComponent } from './_directives';
import { AuthGuard } from './_guards';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AlertService, UserService} from './_services';
import { LoginComponent } from './login';
import { HLSComponent } from './hls';


import { EnvServiceProvider } from './_services/env.service.provider'




@NgModule({
    declarations: [
        AppComponent,
        AlertComponent,    
        LoginComponent,
   
        
        HLSComponent,
       
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        ReactiveFormsModule,
        HttpClientModule,
        AngularDateTimePickerModule,
        CommonModule,
        FormsModule,
        CalendarModule,
        DropdownModule,
        RadioButtonModule,
        InputTextModule,
        TableModule,
        FileUploadModule,
        BrowserAnimationsModule
    ],
    providers: [
        AuthGuard,
        AlertService,
        UserService,
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        EnvServiceProvider
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
