import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



import { LoginComponent } from './login';
import { HLSComponent } from './hls';
import { AuthGuard } from './_guards';

const routes: Routes = [     
    { path: 'login', component: LoginComponent },   
    { path: '', component: HLSComponent, canActivate: [AuthGuard]},  
    { path: '**', redirectTo: '' }              // otherwise redirect to home
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
