'use strict';


var reqConnect;
var reqMessage;
var connectionId = -1;
var pc;

var server = "http://10.0.0.10:8051";
 


function send(type, data)
 {
    var r = new XMLHttpRequest();
    r.open("POST", server + "?" + type,
           true);
    r.setRequestHeader("Content-Type", "text/plain");
    r.setRequestHeader("Pragma", connectionId);
    r.send(data);
    trace('Send(' + type + '):\n' + data + ')');
    r = null;
}

function onIceCandidate(event) {
    trace('onIceCandidate()');
    if (event.candidate) {
        send('candidate', JSON.stringify({
            type: 'candidate',
            sdpMLineIndex: event.candidate.sdpMLineIndex,
            sdpMid: event.candidate.sdpMid,
            candidate: event.candidate.candidate
        }));

    } else {
        trace('End of candidates.');
    }
}

function onRemoteStreamAdded(event) {
    trace('onRemoteStreamAdded()');
    remoteVideo.srcObject = event.stream;
}

function onRemoteStreamRemoved(event) {
    trace('onRemoteStreamRemoved()');
}


function onCreateAnswer(sdp)
{
    trace('onCreateAnswer()');
    pc.setLocalDescription(sdp);
    send("answer", sdp.sdp);
}

function onCreateAnswerError(error) {
    trace('onCreateAnswerError(). Failed to create session description: ' + error.toString());
}

function onSetRemoteDesc()
{
    pc.createAnswer().then(
      onCreateAnswer,
      onCreateAnswerError
    );
}

function onSetRemoteDescError(error) {
    trace('onSetRemoteDescError(). Failed to create session description: ' + error.toString());
}

function CreatePeerConnection(message) {
    trace('CreatePeerConnnection()');
    try {
        //var config = { "iceServers": [{ "url": "stun:stun.l.google.com:19302" }] };
        //pc = new RTCPeerConnection(config);
        pc = new RTCPeerConnection({
            iceServers: [
              {
                  urls: "stun:stun.l.google.com:19302"
              }
            ]
        });
        //pc = new RTCPeerConnection(null);
        pc.onicecandidate = onIceCandidate;
        pc.onaddstream = onRemoteStreamAdded;
        pc.onremovestream = onRemoteStreamRemoved;

        pc.setRemoteDescription(new RTCSessionDescription(message)).then(
          onSetRemoteDesc,
          onSetRemoteDescError
        );
    } catch (e) {
        trace('Failed to create PeerConnection, exception: ' + e.message);
    }
}

function connectCallback() {
    try {
        if (reqConnect.readyState == 4) {
            if (reqConnect.status == 200) {
                connectionId = reqConnect.getResponseHeader("Pragma")
                reqConnect = null;
                if (connectButton != null) {
                    connectButton.disabled = true;
                    disconnectButton.disabled = false;
                }
                GetMessage();
            }
            else {
                trace("Connection error:" + reqConnect.status +
                    " " + reqConnect.statusText);
                if (connectButton != null) {
                    connectButton.disabled = false;
                    disconnectButton.disabled = true;
                }
            }
        }
    } catch (e) {
        trace("connectCallback() error: " + e.description);
    }
}

/*
function disconnect() {
    trace('disconnect()');
    if (connectButton != null) {
        connectButton.disabled = false;
        disconnectButton.disabled = true;
    }

    if (reqConnect) {
        reqConnect.abort();
        reqConnect = null;
    }

    if (connectionId != -1) {
        reqConnect = new XMLHttpRequest();
        reqConnect.open("GET", server + "?disconnect", true);
        reqConnect.setRequestHeader("Pragma", connectionId);
        reqConnect.send();
        reqConnect = null;
        connectionId = -1;
    }

    if (reqMessage) {
        reqMessage.abort();
        reqMessage = null;
    }

    if (pc != null)
    {
        pc.close();
    }
}
*/
//////////////////
function GetMessage() {
    trace('GetMessage()...');
    try {
        reqMessage = new XMLHttpRequest();
        reqMessage.onreadystatechange = GetMessageCallback;
        reqMessage.ontimeout = onGetMessageTimeout;
        reqMessage.open("GET", server + "?wait", true);
        reqMessage.setRequestHeader("Pragma", connectionId);
        reqMessage.send();
    } catch (e) {
        trace("GetMessage() error: " + e.description);
    }
}

function onGetMessageTimeout() {
    trace('GetMessageTimeout()');
    reqMessage.abort();
    reqMessage = null;
    window.setTimeout(GetMessage, 0);
}

function GetMessageCallback() {
    try {
        if (reqMessage.readyState != 4)
            return;
        if (reqMessage.status != 200) {
            trace("GetMessageCallback() error:" + reqMessage.status +
                                " " + reqMessage.statusText);
            disconnect();
        }
        else {
            handleGetMessage(reqMessage.responseText)
        }

        if (reqMessage) {
            reqMessage.abort();
            reqMessage = null;
        }

        if (connectionId != -1)
            window.setTimeout(GetMessage, 0);
    } catch (e) {
        trace("GetMessageCallback() error: " + e.description);
    }
}

function handleGetMessage(data) {
    var dataJson;
    try {
        dataJson = JSON.parse(data);
    }
    catch (e) {
        trace("handleGetMessage() error: " + e.description);
    }
    if (dataJson.candidate)
    {
        trace("handleGetMessage(), received:\n" + dataJson.candidate);
        var candidate = new RTCIceCandidate({ sdpMLineIndex: dataJson.sdpMLineIndex, candidate: dataJson.candidate });
        pc.addIceCandidate(candidate);
    }
    else if (dataJson.sdp)
    {
        trace("handleGetMessage(), received:\n" + dataJson.sdp);
        CreatePeerConnection(dataJson);
    }
}

function connect() 
{
     
    try {
        //if (serverInput) {
          //  server = serverInput.value;
        //}
        trace("Connecting to: " + server);
        reqConnect = new XMLHttpRequest();
        reqConnect.onreadystatechange = connectCallback;
        reqConnect.open("GET", server + "?connect", true);
        reqConnect.send();
    }
    catch (e) {
        trace("connect() error: " + e.description);
    }
}

alert("ee");
window.onload = connect();


//