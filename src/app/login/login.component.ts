import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService, UserService } from '../_services';

@Component({templateUrl: 'login.component.html'})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    returnUrl: string;
    selectedValue: string = 'observer';
    submitted: boolean;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService,
        private alertService: AlertService,
        ) {}

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            UserName: ['', Validators.required],
            Password: ['', Validators.required]
           
        });

        if (localStorage.getItem('mpVideoServerUser')) {
    
       
        }
        this.userService.logout();

        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    get f() { return this.loginForm.controls; }

    onSubmit() {

        if (this.loginForm.invalid) {
            return;
        }  
        /*console.log("Inside login");
        localStorage.setItem('mpVideoServerUser', JSON.stringify('AfzalAhmad'));
        this.router.navigate(['./hls']);*/
  

        this.userService.login(this.loginForm.value)
            .subscribe(
                    response =>{
                    this.alertService.success('User Authenticated', true);
                    localStorage.setItem('mpVideoServerUser', JSON.stringify(response.body['userName']));
                    localStorage.setItem('mpVideoServerToken', JSON.stringify(response.body['access_token']));
                    this.router.navigate(['/hls']);                  
                   },
                error => this.alertService.error(error, false)
            );
    }
}
