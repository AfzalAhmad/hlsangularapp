﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { User } from '../_models';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { EnvService } from '../_services/env.service'

@Injectable()
export class UserService {
    constructor(private http: HttpClient, private env: EnvService) { }

   

    login(user: User) {
        return this.http.post(`${this.env.apiUrl}/api/Account/Login`, user, { observe: 'response' });
    }
   

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('mpVideoServerUser');
        localStorage.removeItem('mpVideoServerToken');     
       
    }

}
