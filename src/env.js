(function (window) {
    window.__env = window.__env || {};
  
    // API url
    window.__env.apiUrl = 'http://10.0.0.10:88/megapopserver/';    
    window.__env.nginxUrl = 'http://localhost:8080/';
  
    // Whether or not to enable debug mode
    // Setting this to false will disable console output
    window.__env.enableDebug = true;
  }(this));